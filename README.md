# turing-ecom-api

This app was initialized using sailjs.
I spent around 21 hours on this till now from setting up to development.

To run the project,

Used `sails` as a backend framework
Used `joi` for input validation

Please check `./postman-collection-apis.json`

use npm >= 8

1. npm install -g sails
2. npm install
3. sails lift

server will run on `localhost:1337`

The project is not completed because I have fallen sick midway and now can't spend
anymore time on it. I could have completed it had I been well.

I have implemented 21 routes with the mentioned inputs and outpoints

Implemented routes can be found @ `config/routes.js`
List of error message can be found @ `config/errorMessages.js`


Database is deployed -> please check `config/datastores`
Run the app locally and make the requests

