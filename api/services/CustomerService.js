/**
 * CustomerService.js
 *
 * @description :: Helper methods for CustomerController
 * @help        :: See http://sailsjs.org/documentation/concepts/services
 */
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  
  login: (email, password) => {

    let customer;
    let expTime = '24h';

    return Customer.findOne({email: email})
      .then(customerFound => {
        if (!customerFound) throw new Error('CustomerNotFound');
        customer = customerFound;

        return bcrypt.hash(password, customer.salt);
      })
      .then(hash => {
        if (hash !== customer.hash) throw new Error('InvalidPassword');

        // secret key below should be coming from environment current arrangement is temporary
        let secretKey = 'somesecretKey';

        return jwt.sign({customer}, secretKey, { expiresIn: expTime });
      })
      .then(token => {
        return {
          customer: customer,
          accessToken: token,
          expires_in: expTime
        };
      })
  },

};
