/**
 * Validator.js
 *
 * @description :: Helper to validate JSON request body
 * @help        :: See http://sailsjs.org/documentation/concepts/services
 */
const Promise = require('bluebird');
const Joi = require('joi');

// internal helpers
const validateAsync = Promise.promisify(Joi.validate);

module.exports = {
  /**
   * Exposed the Joi so that we can use it when defining validation schema
   */
  Schema: Joi,

  validate: function (body, schema) {
    const options = {
      abortEarly: false
    };

    return validateAsync(body, schema, options);
  }
};
