/**
 * isAuthenticated
 * @module      :: Policy
 * @description :: Validate access token
 */

const Promise = require('bluebird');
const jwt = Promise.promisifyAll(require('jsonwebtoken'));

module.exports = function (req, res, next) {

  let secretKey = 'somesecretKey';
  const bearerHeader = req.headers['user-key'];

  if (!bearerHeader) {
    return res.status(403).json({
      error: {
        code: sails.config.errorMessages['InvalidTokenType'].code,
        message: sails.config.errorMessages['InvalidTokenType'].message
      }
    })
  }

  const bearer = bearerHeader.split(' ');
  const bearerToken = bearer[1];

    jwt.verify(bearerToken, secretKey, (err, authData) => {
      if(err) {
        sails.log.error(err);

        if (err.name === 'JsonWebTokenError') {
          return res.status(403).json({
            error: {
              code: sails.config.errorMessages[err.name].code,
              message: sails.config.errorMessages[err.name].message
            }
          })
        }

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
          }
        });
      } 
      else {
        req.authData = authData;
        return next();
      }
    });
};
