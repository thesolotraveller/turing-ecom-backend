/**
 * Attribute.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: {
      type: 'number',
      columnName: 'attribute_id',
      required: true
    },

    name: {
      type: 'string'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.attribute_id = obj.id;
    delete obj.id;
    return obj;
  }

};

