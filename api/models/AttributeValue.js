/**
 * AttributeValue.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'attribute_value',

  attributes: {

    id: {
      type: 'number',
      columnName: 'attribute_value_id',
      required: true
    },

    attribute_id: {
      model: 'attribute'
    },

    value: {
      type: 'string'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.attribute_value_id = obj.id;
    delete obj.id;
    return obj;
  }

};
