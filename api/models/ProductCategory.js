/**
 * ProductCategory.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'product_category',

  attributes: {

    id: {
      type: 'number',
      columnName: 'product_id',
      required: true
    },

    category_id: {
      model: 'Category'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.product_id = obj.id;
    delete obj.id;
    return obj;
  }

};
