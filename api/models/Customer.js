/**
 * Customer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const _ = require('lodash');

module.exports = {

  attributes: {

    id: {
      type: 'number',
      columnName: 'customer_id',
      required: true
    },

    name: {
      type: 'string'
    },

    email: {
      type: 'string'
    },

    address_1: {
      type: 'string',
      defaultsTo: ''
    },

    address_2: {
      type: 'string',
      defaultsTo: ''
    },

    city: {
      type: 'string',
      defaultsTo: ''
    },

    region: {
      type: 'string',
      defaultsTo: ''
    },

    postal_code: {
      type: 'string',
      defaultsTo: ''
    },

    country: {
      type: 'string',
      defaultsTo: ''
    },

    shipping_region_id: {
      type: 'number',
      defaultsTo: 1
    },

    day_phone: {
      type: 'string',
      defaultsTo: ''
    },

    eve_phone: {
      type: 'string',
      defaultsTo: ''
    },

    mob_phone: {
      type: 'string',
      defaultsTo: ''
    },

    salt: {
      type: 'string',
      defaultsTo: ''
    },

    hash: {
      type: 'string',
      defaultsTo: ''
    }

  },

  customToJSON: function () {
    let obj = this;
    obj.customer_id = obj.id;
    delete obj.salt;
    delete obj.hash;
    let res = _.clone(obj);
    delete res.id;
    return res;
  }

};
