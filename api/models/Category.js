/**
 * Product.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: {
      type: 'number',
      columnName: 'category_id',
      required: true
    },

    name: {
      type: 'string'
    },

    description: {
      type: 'string'
    },

    department_id: {
      model: 'department'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.category_id = obj.id;
    delete obj.id;
    return obj;
  }

};