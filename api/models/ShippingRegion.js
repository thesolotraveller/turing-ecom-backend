/**
 * ShippingRegion.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'shipping_region',

  attributes: {

    id: {
      type: 'number',
      columnName: 'shipping_region_id',
      required: true
    },

    shipping_region: {
      type: 'string'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.shipping_region_id = obj.id;
    delete obj.id;
    return obj;
  }

};