/**
 * Cart.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const moment = require('moment');

module.exports = {

  tableName: 'shopping_cart',

  attributes: {

    id: { type: 'number', columnName: 'item_id', autoIncrement: true },
    cart_id: { type: 'string' },
    product_id: { type: 'number' },
    attributes: { type: 'string' },
    quantity: { type: 'number' },
    buy_now: { type: 'number' },
    added_on: { type: 'string', columnType: 'datetime'}

  },

  customToJSON: function () {
    var obj = this;
    obj.cart_id = obj.id;
    delete obj.id;
    return obj;
  }

};