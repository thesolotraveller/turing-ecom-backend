/**
 * ProductAttribute.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'product_attribute',

  attributes: {

    id: {
      type: 'string',
      columnName: 'product_id',
      required: true
    },

    attribute_value_id: {
      model: 'AttributeValue'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.product_id = obj.id;
    delete obj.id;
    return obj;
  }

};

