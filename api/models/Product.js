/**
 * Product.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: {
      type: 'number',
      columnName: 'product_id',
      autoIncrement: true
    },

    name: {
      type: 'string'
    },

    description: {
      type: 'string'
    },

    price: {
      type: 'number'
    },

    discounted_price: {
      type: 'number'
    },

    thumbnail: {
      type: 'string'
    },

    image: {
      type: 'string'
    },

    image_2: {
      type: 'string'
    },

    display: {
      type: 'number'
    }

  },

  customToJSON: function () {
    var obj = this;
    obj.product_id = obj.id;
    delete obj.id;
    return obj;
  }

};