/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const Promise = require('bluebird');

module.exports = {

  find: (req, res) => {

    let {page, limit, description_length} = req.query;

    // setting default values for common query string params
    page = !page ? 1 : page;
    limit = !limit ? 20 : limit;
    description_length = !description_length ? 200 : description_length;

    return Product.find()
      .paginate(parseInt(page-1), parseInt(limit))
      .then(products => {

        // filtering the results on the basis of maximum length of description that is allowed
        const filteredProducts = products.filter(eachProduct => eachProduct.description.length <= description_length);

        return res.ok({
          count: filteredProducts.length,
          rows: filteredProducts
        })
      })
      .catch(err => {
        sails.log.error(err);

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      })
  },

  findOne: (req, res) => {

    let productId = req.param('product_id');

    return Product.findOne({id: productId})
      .then(product => {
        if (!product) throw new Error('ResourceNotFoundForId');
        res.ok(product);
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(401).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  },

  categoryProducts: (req, res) => {

    let categoryId = req.param('category_id');
    let {page, limit, description_length} = req.query;

    // setting default values for common query string params
    page = !page ? 1 : page;
    limit = !limit ? 20 : limit;
    description_length = !description_length ? 200 : description_length;

    return ProductCategory.find({category_id: categoryId})
      .then(productCategories => {
        if (productCategories.length === 0) throw new Error('ResourceNotFoundForId');

        const productIds = productCategories.map(eachCategory => eachCategory.id);

        // return Promise.all(CategoryProductsFetchPromiseArray);
        return Product
          .find({ id: productIds })
          .paginate(parseInt(page-1), parseInt(limit));
      })
      .then(products => {

        // filtering the results on the basis of maximum length of description that is allowed
        const filteredProducts = products.filter(eachProduct => eachProduct.description.length <= description_length);

        res.ok({
          count: filteredProducts.length,
          rows: filteredProducts
        })
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(401).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      })
  },

  departmentProducts: (req, res) => {

    let departmentId = req.param('department_id');
    let {page, limit, description_length} = req.query;

    // setting default values for common query string params
    page = !page ? 1 : page;
    limit = !limit ? 20 : limit;
    description_length = !description_length ? 200 : description_length;

    return Category.find({department_id: departmentId})
      .then(categories => {
        if (categories.length === 0) throw new Error('ResourceNotFoundForId');

        const categoryIds = categories.map(eachCategory => eachCategory.id);
        return ProductCategory.find({category_id: categoryIds})
      })
      .then(productCategories => {
        if (productCategories.length === 0) throw new Error('ResourceNotFoundForId');

        const productIds = productCategories.map(eachCategory => eachCategory.id);

        // return Promise.all(CategoryProductsFetchPromiseArray);
        return Product
          .find({ id: productIds })
          .paginate(parseInt(page-1), parseInt(limit));
      })
      .then(products => {

        // filtering the results on the basis of maximum length of description that is allowed
        const filteredProducts = products.filter(eachProduct => eachProduct.description.length <= description_length);

        res.ok({
          count: filteredProducts.length,
          rows: filteredProducts
        })
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(401).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      })
  },

};
