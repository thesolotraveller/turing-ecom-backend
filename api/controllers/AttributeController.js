/**
 * AttributeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const Promise = require('bluebird');

module.exports = {

  find: (req, res) => {

    return Attribute.find()
      .then(attributes => res.ok(attributes))
      .catch(err => {
        sails.log.error(err.code, err.message);

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      })
  },

  findOne: (req, res) => {

    let attributeId = req.param('attribute_id');

    return Attribute.findOne({id: attributeId})
      .then(attribute => {
        if (!attribute) throw new Error('ResourceNotFoundForId');
        res.ok(attribute);
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(400).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  },

  findAttributeValues: (req, res) => {

    let attributeId = req.param('attribute_id');

    return AttributeValue.find({
        select: ['id', 'value'],
        where: { attribute_id: attributeId}
      })
      .then(attribute => {
        if (!attribute) throw new Error('ResourceNotFoundForId');
        res.ok(attribute);
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(400).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  },

  findProductAttributes: (req, res) => {

    let productId = req.param('product_id');

    return ProductAttribute.find({id: productId})
      .then(productAttributes => {
        if (productAttributes.length === 0) throw new Error('ResourceNotFoundForId');

        let ProductAttributeFetchPromiseArray = [];
        
        productAttributes.forEach(eachAttr => {
          let eachPromise = AttributeValue.findOne({id: eachAttr.attribute_value_id}).populate('attribute_id');
          ProductAttributeFetchPromiseArray.push(eachPromise);
        })

        return Promise.all(ProductAttributeFetchPromiseArray);
      })
      .then(productAttributes => {

        // constructing the result in the required format
        productAttributes.forEach(eachAttr => {
          eachAttr.attribute_value = eachAttr.value;
          delete eachAttr.value;
          eachAttr.attribute_name = eachAttr.attribute_id.name;
          delete eachAttr.attribute_id;
        })

        res.ok(productAttributes);
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(400).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })

  }

};