/**
 * CartController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const uuidv1 = require('uuid/v1');
const _ = require('lodash');
const moment = require('moment');

module.exports = {
  
  getUniqueId: (req, res) => res.ok(uuidv1()),

  addProductToCart: (req, res) => {

    let body, productBody, addProductBody = {};

    let schema = ValidatorService.Schema.object().keys({
      cart_id: ValidatorService.Schema.string().required().label('Cart Id'),
      product_id: ValidatorService.Schema.string().required().label('Product Id'),
      attributes: ValidatorService.Schema.string().required().label('Attributes')
    });

    ValidatorService.validate(req.body, schema)
      .then(validatedRequestBody => {
        body = validatedRequestBody;

        return Product.findOne({id: body.product_id});
      })
      .then(product => {
        productBody = _.clone(product);

        addProductBody.cart_id = body.cart_id;
        addProductBody.attributes = body.attributes;        
        addProductBody.product_id = product.product_id ? product.product_id : product.id;
        addProductBody.quantity = 1;
        addProductBody.buy_now = 1;
        addProductBody.added_on = moment().utc().format().slice(0, -1);

        return Cart.count();
      })
      .then(count => {
        addProductBody.id = ++count;

        return Cart.create(addProductBody);
      })
      .then(() => {
        console.log(addProductBody);

        return res.ok([{
          item_id: addProductBody.item_id,
          name: productBody.name,
          attributes: addProductBody.attributes,
          price: productBody.price,
          quantity: 1,
          subTotal: productBody.price
        }]);
      })
      .catch(err => {
        sails.log.error(err);

        if (err.name === 'ValidationError') {
          return res.json(400, {
            message: 'Validation error',
            errors: err.details
          });
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  }

};

