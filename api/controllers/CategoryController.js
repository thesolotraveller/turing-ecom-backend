/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const _ = require('lodash');

module.exports = {

    find: (req, res) => {
  
      let {page, limit, order} = req.query;
  
      // setting default values for common query string params
      page = !page ? 1 : page;
      limit = !limit ? 20 : limit;

      // here we are changing supplied input order 'name,DESC' to 'name DESC
      order = _.replace(order, new RegExp(",","g")," ");
  
      return Category.find({ sort: order })
        .paginate(parseInt(page-1), parseInt(limit))
        .then(categories => {
  
          return res.ok({
            count: categories.length,
            rows: categories
          })
        })
        .catch(err => {
          sails.log.error(err.code, err.message);

          if (err.code === 'E_INVALID_CRITERIA') {
            return res.status(400).json({
              error: {
                message: err.message,
                code: err.code
              }
            });
          }
  
          return res.serverError({
            error: {
              message: "Sorry, there was an error in serving your the requested resource",
              internalMessage: err.details,
              code: err.code,
              status: 500
            }
          });
        })
    },

    findOne: (req, res) => {

      let categoryId = req.param('category_id');
  
      return Category.findOne({id: categoryId})
        .then(category => {
          if (!category) throw new Error('ResourceNotFoundForId');
          res.ok(category);
        })
        .catch(err => {
          sails.log.error(err);
  
          if (err.message === 'ResourceNotFoundForId') {
            return res.status(401).json({
              error: {
                code: sails.config.errorMessages[err.message].code,
                message: sails.config.errorMessages[err.message].message
              }
            })
          }
  
          return res.status(500).json({
            error: {
              code: err.code,
              message: "Sorry, there was an error in serving you the requested resource"
            }
          });
        })
    },

    productCategories: (req, res) => {

      let productId = req.param('product_id');

      return ProductCategory.findOne({id: productId})
        .then(productCategory => {
          if (!productCategory) throw new Error('ResourceNotFoundForId');
          console.log(productCategory.category_id);
          return Category.find({
            select: ['name', 'department_id'],
            where: {id: productCategory.category_id}
          });
        })
        .then(productCategory => res.ok(productCategory))
        .catch(err => {
          sails.log.error(err);
  
          if (err.message === 'ResourceNotFoundForId') {
            return res.status(401).json({
              error: {
                code: sails.config.errorMessages[err.message].code,
                message: sails.config.errorMessages[err.message].message
              }
            })
          }
  
          return res.status(500).json({
            error: {
              code: err.code,
              message: "Sorry, there was an error in serving you the requested resource"
            }
          });
        })

    },

    departmentCategories: (req, res) => {

      let departmentId = req.param('department_id');
  
      return Category.find({department_id: departmentId})
        .then(categories => res.ok(categories))
        .catch(err => {
          sails.log.error(err);
  
          return res.status(500).json({
            error: {
              code: err.code,
              message: "Sorry, there was an error in serving you the requested resource"
            }
          });
        })
    }
  
  };
  