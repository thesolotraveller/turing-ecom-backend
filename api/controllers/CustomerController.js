/**
 * CustomerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const Promise = require('bluebird');
const brcrypt = Promise.promisifyAll(require('bcrypt'));

module.exports = {

  create: function (req, res) {

    let body;
    let password;

    let schema = ValidatorService.Schema.object().keys({
      name: ValidatorService.Schema.string().min(1).required().label('Customer Name'),
      email: ValidatorService.Schema.string().label('Email'),
      password: ValidatorService.Schema.string().label('Password')
    });

    ValidatorService.validate(req.body, schema)
      .then(validatedRequestBody => {
        body = validatedRequestBody;
        password = body.password;

        return Customer.find({email: body.email})
      })
      .then(customers => {
        if (customers.length > 0) throw new Error('CustomerAlreadyExists');

        return brcrypt.genSalt(10);
      })
      .then(salt => {
        body.salt = salt;

        return brcrypt.hash(body.password, body.salt);
      })
      .then(hash => {
        body.hash = hash;

        return Customer.count();
      })
      .then(count => {
        body.id = ++count;
        delete body.password;
        
        return Customer.create(body);
      })
      .then(() => CustomerService.login(body.email, password))
      .then(loginResponse => res.ok(loginResponse))
      .catch(function (err) {
        sails.log.error(err);

        if (err.name === 'ValidationError') {
          return res.json(400, {
            message: 'Validation error',
            errors: err.details
          });
        }

        if (['CustomerAlreadyExists'].indexOf(err.message > -1)) {
          return res.status(403).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      });
  },

  login: (req, res) => {

    let schema = ValidatorService.Schema.object().keys({
      email: ValidatorService.Schema.string().label('Email'),
      password: ValidatorService.Schema.string().label('Password')
    });

    ValidatorService.validate(req.body, schema)
      .then(validatedRequestBody => {
        body = validatedRequestBody;

        return CustomerService.login(body.email, body.password);
      })
      .then(loginResponse => res.ok(loginResponse))
      .catch(function (err) {
        sails.log.error(err);

        if (err.name === 'ValidationError') {
          return res.json(400, {
            message: 'Validation error',
            errors: err.details
          });
        }

        if (['InvalidPassword', 'CustomerNotFound'].indexOf(err.message > -1)) {
          return res.status(403).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving your the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      });
  },

  findOne: (req, res) => {

    let customerId = req.authData.customer.customer_id;

    return Customer.findOne({id: customerId})
      .then(customer => res.ok(customer))
      .catch(err => {
        sails.log.error(err);

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })

  },

  update: (req, res) => {

    let customerId = req.authData.customer.customer_id;
    let customerSalt = req.authData.customer.salt;
    let customerEmail = req.authData.customer.email;

    let body;

    let schema = ValidatorService.Schema.object().keys({
      name: ValidatorService.Schema.string().required().label('Customer Name'),
      email: ValidatorService.Schema.string().required().label('Email'),
      password: ValidatorService.Schema.string().label('Password'),
      day_phone: ValidatorService.Schema.string().label('Day Phone'),
      eve_phone: ValidatorService.Schema.string().label('Eve Phone'),
      mobile_phone: ValidatorService.Schema.string().label('Mobile Phone')
    });

    ValidatorService.validate(req.body, schema)
      .then(validatedRequestBody => {
        body = validatedRequestBody;
        
        return Customer.find({email: body.email});
      })
      .then(foundCustomer => {
        if (foundCustomer.length > 0 && foundCustomer[0].email !== customerEmail) {
          throw new Error('CustomerAlreadyExists');
        }
        
        if (body.password) {
          return brcrypt.hash(body.password, customerSalt);
        }

        return null;
      })
      .then(hash => {
        if (hash) {
          body.hash = hash;
        }

        return Customer.update({id: customerId},body);
      })
      .then(() => res.ok())
      .catch(err => {
        sails.log.error(err);

        if (err.name === 'ValidationError') {
          return res.json(400, {
            message: 'Validation error',
            errors: err.details
          });
        }

        if (['CustomerAlreadyExists'].indexOf(err.message > -1)) {
          return res.status(403).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  },

  updateAddress: (req, res) => {

    let customerId = req.authData.customer.customer_id;

    let body;

    let schema = ValidatorService.Schema.object().keys({
      address_1: ValidatorService.Schema.string().required().label('Address 1'),
      address_2: ValidatorService.Schema.string().label('Address 1'),
      city: ValidatorService.Schema.string().required().label('City'),
      region: ValidatorService.Schema.string().required().label('Region'),
      postal_code: ValidatorService.Schema.string().required().label('Postal Code'),
      country: ValidatorService.Schema.string().required().label('Country'),
      shipping_region_id: ValidatorService.Schema.number().required().label('Shipping Region Id')
    });

    ValidatorService.validate(req.body, schema)
      .then(validatedRequestBody => {
        body = validatedRequestBody;

        return ShippingRegion.findOne({id: body.shipping_region_id});
      })
      .then(shippingRegion => {
        if (!shippingRegion) throw new Error('InvalidShippingRegionId');

        return Customer.update({id: customerId}, body);
      })
      .then(() => res.ok())
      .catch(err => {
        sails.log.error(err);

        if (err.name === 'ValidationError') {
          return res.json(400, {
            message: 'Validation error',
            errors: err.details
          });
        }

        if (err.message === 'InvalidShippingRegionId') {
          return res.status(401).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  }

};
