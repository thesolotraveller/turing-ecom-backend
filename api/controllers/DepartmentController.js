/**
 * DepartmentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  find: (req, res) => {

    return Department.find()
      .then(departments => {
        return res.ok({
          count: departments.length,
          rows: departments
        })
      })
      .catch(err => {
        sails.log.error(err);

        return res.serverError({
          error: {
            message: "Sorry, there was an error in serving you the requested resource",
            internalMessage: err.details,
            code: err.code,
            status: 500
          }
        });
      })
  },

  findOne: (req, res) => {

    let departmentId = req.param('department_id');

    return Department.findOne({id: departmentId})
      .then(department => {
        if (!department) throw new Error('ResourceNotFoundForId');
        res.ok(department);
      })
      .catch(err => {
        sails.log.error(err);

        if (err.message === 'ResourceNotFoundForId') {
          return res.status(401).json({
            error: {
              code: sails.config.errorMessages[err.message].code,
              message: sails.config.errorMessages[err.message].message
            }
          })
        }

        return res.status(500).json({
          error: {
            code: err.code,
            message: "Sorry, there was an error in serving you the requested resource"
          }
        });
      })
  }

};
