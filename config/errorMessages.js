module.exports.errorMessages = {
  ResourceNotFoundForId: {code: 'INVALID_ID', message: 'No resource found with this id'},
  InvalidPassword: {code: 'INVALID_PASSWORD', message: 'Invalid password entered'},
  CustomerNotFound: {code: 'INVALID_EMAIL', message: 'No customer found with this email'},
  CustomerAlreadyExists: {code: 'CUSTOMER_ALREADY_EXISTS', message: 'Email already registered for a customer'},
  JsonWebTokenError: {code: 'JsonWebTokenError', message: 'Authentication failed because of incorrect token'},
  InvalidTokenType: {code: 'INVALID_TOKEN_TYPE', message: 'Auth token missing or supplied in improper format'},
  InvalidShippingRegionId: {code: 'INVALID_SHIPPING_REGION_ID', message: 'No shipping region found with this id'}
};