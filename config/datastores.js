/**
 * Datastores
 * (sails.config.datastores)
 *
 * A set of datastore configurations which tell Sails where to fetch or save
 * data when you execute built-in model methods like `.find()` and `.create()`.
 *
 *  > This file is mainly useful for configuring your development database,
 *  > as well as any additional one-off databases used by individual models.
 *  > Ready to go live?  Head towards `config/env/production.js`.
 *
 * For more information on configuring datastores, check out:
 * https://sailsjs.com/config/datastores
 */

module.exports.datastores = {

  // here new_schema is the name of the database for now
  // we can over ride that in config/local.js
  // but that file is not pushed

  // default: {
  //   adapter: require('sails-mysql'),
  //   url: 'mysql://root:password@localhost:3306/turing_ecom',
  // }


  // You can use my database
  // just need to run the app locally
  default: {
    adapter: require('sails-mysql'),
    url: 'mysql://root:password@turingecom.ceuysbhcuxpo.us-east-1.rds.amazonaws.com:3306/turingecom',
  }

};
