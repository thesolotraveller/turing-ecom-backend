/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/

  // department routes
  'GET /departments': 'DepartmentController.find',
  'GET /departments/:department_id': 'DepartmentController.findOne',

  // category routes
  'GET /categories': 'CategoryController.find',
  'GET /categories/:category_id': 'CategoryController.findOne',
  'GET /categories/inProduct/:product_id': 'CategoryController.productCategories',
  'GET /categories/inDepartment/:department_id': 'CategoryController.departmentCategories',

  // attribute routes
  'GET /attributes': 'AttributeController.find',
  'GET /attributes/:attribute_id': 'AttributeController.findOne',
  'GET /attributes/values/:attribute_id': 'AttributeController.findAttributeValues',
  'GET /attributes/inProduct/:product_id': 'AttributeController.findProductAttributes',

  // product routes
  'GET /products': 'ProductController.find',
  'GET /products/:product_id': 'ProductController.findOne',
  'GET /products/inCategory/:category_id': 'ProductController.categoryProducts',
  'GET /products/inDepartment/:department_id': 'ProductController.departmentProducts',

  // customer routes
  'POST /customers': 'CustomerController.create',
  'POST /customers/login': 'CustomerController.login',
  'GET /customer': 'CustomerController.findOne',
  'POST /customer': 'CustomerController.update',
  'POST /customers/address': 'CustomerController.updateAddress',

  // shopping cart routes
  'GET /shoppingCart/generateUniqueId': 'CartController.getUniqueId',
  'POST /shoppingCart/add': 'CartController.addProductToCart'
};
